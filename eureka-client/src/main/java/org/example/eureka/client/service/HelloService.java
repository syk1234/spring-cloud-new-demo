package org.example.eureka.client.service;

import org.example.eureka.client.vo.User;

/**
 * 代码描述<p>
 *
 * @author xuker
 * @since 2021/5/28 下午2:34
 */
public interface HelloService {

  String hello();

  String hello(String name);

  User hello(String name, Integer age);

  String hello(User user);

  String helloHystrix();

  String config();
}
