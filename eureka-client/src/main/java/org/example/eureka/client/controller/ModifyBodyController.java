package org.example.eureka.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@Slf4j
public class ModifyBodyController {

    @RequestMapping(value = "/modify-request/hello", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> modifyRequest(@RequestBody Map<String, String> map) {
        return map;
    }

    @RequestMapping(value = "/modify-response/hello", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> modifyResponse(@RequestBody Map<String, String> map) {
        return map;
    }
}
