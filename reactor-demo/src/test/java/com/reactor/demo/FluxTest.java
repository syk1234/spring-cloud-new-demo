package com.reactor.demo;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Arrays;
import java.util.function.Consumer;

public class FluxTest {

    /**
     * 基本用法
     */
    @Test
    public void test1() {
        //通过just传入可变的参数，依次输出
        Flux.just("hello", "world", "just").doOnNext(System.out::println)
                .doOnComplete(() -> System.out.println("just over")).subscribe();
        //传入一个范围
        Flux.range(100, 10)
                .doOnNext(System.out::println).doOnComplete(() -> System.out.println("OK")).subscribe();
        //传入list
        Flux.fromIterable(Arrays.asList("01", "02", "03")).doOnNext(System.out::println).subscribe();
        //传入一个数组
        Flux.fromArray(new Object[]{"obj1", "obj2"}).doOnNext(System.out::println).subscribe();
    }


    /**
     * 处理空值
     */
    @Test
    public void testEmpty() {
        //如果序列是个空的，就给个默认值
        Flux.empty().defaultIfEmpty(1).doOnNext(System.out::println).subscribe();
        //如果序列是空的，就用新序列代替
        Flux.empty().switchIfEmpty(Mono.just("100")).doOnNext(System.out::println).subscribe();
    }


    /**
     * 序列在执行时的一些监听方法doOnXXXX
     */
    @Test
    public void testDoOn() {
        System.out.println("----------");
        Flux.range(100, 10)
                .doOnNext(System.out::println).doOnComplete(() -> System.out.println("OK"));

        System.out.println("----------");
        Flux.range(100, 10).doFirst(() -> System.out.println("第一个执行开始")).subscribe();

        System.out.println("----------");
        Flux.range(100, 10).doFinally(it -> System.out.println("终止信号的类型为" + it.name())).subscribe();

        System.out.println("----------");
        Flux.range(100, 10).doOnSubscribe(it -> System.out.println("该序列已被订阅")).subscribe();

        System.out.println("----------");

        Flux.range(100, 10).doOnRequest(value -> System.out.println("doOnRequest:" + value)).subscribe();

        //在完成或者error时，也就是序列终止时执行runnable
        System.out.println("----------");
        Flux.range(100, 10).doOnTerminate(() -> System.out.println("doOnTerminate")).subscribe();

        //doOnEach每次向下游传播，都会得到一个信号类型，可以根据该信号类型执行一些操作
        System.out.println("----------");
        Flux.range(100, 10).doOnEach(it -> System.out.println("doOnEach:" + it)).subscribe();
    }


    /**
     * filter用法
     */
    @Test
    public void testFilter() {

        System.out.println("----------");
        //将上游的数据进行类型判断，符合该类型的数据将流向下游
        Flux.just(new Object(), "Hello", 1)
                .ofType(String.class).doOnNext(System.out::println)
                .doOnComplete(() -> System.out.println("过滤String示例")).subscribe();

        System.out.println("----------");
        //过滤数据
        Flux.range(100, 10)
                .filter(it -> it > 105)
                .doOnComplete(() -> System.out.println("取出大于105示例")).subscribe();

        System.out.println("----------");
        //将重复数据过滤,重复数据在整个序列中只保留一个
        Flux.range(100, 10)
                .concatWith(Flux.just(100, 100, 100))
                .distinct().doOnNext(System.out::println)
                .doOnComplete(() -> System.out.println("去除重复数字示例")).subscribe();

        System.out.println("----------");

        //将后来的重复数据过滤，如下，第二个flux拼接到第一个序列时，只会把第二个元素本身的重复元素过滤
        Flux.range(100, 10)
                .concatWith(Flux.just(100, 100, 100))
                .distinctUntilChanged().doOnNext(System.out::println)
                .doOnComplete(() -> System.out.println("将后来的重复数据过滤")).subscribe();

        System.out.println("----------");
        //在序列的开始获取5个元素，
        // limitRequest为true时，则不管该序列会发射多少元素，该参数会向上传递背压，则上游序列只会发出设定的5个元素
        //为false时，则不控制上有元素可以发出N个元素
        Flux.range(100, 10).take(5, false)
                .doOnComplete(() -> System.out.println("在序列的开始获取5个元素")).subscribe();

        System.out.println("----------");
        //参数为时间单位，意味着take获取元素，只会在该时间限制内获取。
        Flux.range(100, 10).take(Duration.ofSeconds(10))
                .doOnNext(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) {
                        System.out.println("当前时间戳为：" + System.currentTimeMillis() + "，数字为：" + integer);
                    }
                })
                .doOnComplete(() -> System.out.println("在指定时间内获取元素"))
                .subscribe(System.out::println);

        System.out.println("----------");
        //获取最后的N位元素
        Flux.range(100, 10).takeLast(2)
                .doOnComplete(() -> System.out.println("获取最后的2位元素"))
                .subscribe(System.out::println);

        System.out.println("----------");
        //获取元素，知道符合条件后停止向下游发送数据,包括条件本身，也就是当it>105的元素也会被发布至下游
        Flux.range(100, 10).takeUntil(it -> it > 105)
                .doOnComplete(() -> System.out.println("一直取数，直到大于105结束"))
                .subscribe(System.out::println);

        System.out.println("----------");
        //获取元素，当元素符合该断言时，如果不符合直接终止,不包含条件本身
        Flux.range(100, 10).takeWhile(it -> it < 105)
                .doOnComplete(() -> System.out.println("取出小于105示例"))
                .subscribe(System.out::println);

        System.out.println("----------");
        //获取指定某个位置的一个元素
        Flux.range(100, 10).elementAt(0)
                .doOnSuccess(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer i) {
                        System.out.println("获取指定某个位置的一个元素:" + i);
                    }
                })
                .subscribe();

        System.out.println("----------");
        //获取最后一个元素，last()如果为空则抛出异常，last(1)如果为空则发出默认值
        Flux.range(100, 10)
                .takeWhile(it -> it > 105).last(1)
                .subscribe(System.out::println);

        System.out.println("----------");
        //跳至第几秒开始执行
        Flux.range(100, 10)
                .skip(Duration.ofSeconds(5)).subscribe(System.out::println);

        System.out.println("----------");
        //跳至第几个元素开始执行
        Flux.range(100, 10)
                .skip(5).subscribe(System.out::println);

        System.out.println("----------");
        //从开始跳到最后第N个元素结束
        Flux.range(100, 10).skipLast(5).subscribe(System.out::println);

        System.out.println("----------");
        //跳至满足条件的地方开始执行,从第一个元素开始，知道满足条件，开始发送至下游
        Flux.range(100, 10).skipUntil(it -> it > 105).subscribe(System.out::println);

        System.out.println("----------");
        //每隔一段时间抽取样本数(取在这个时间的最后一个元素)，如果相隔实现大于序列的执行时间，则去最后一元素
        Flux.range(100, 100000000).sample(Duration.ofMillis(100)).subscribe(System.out::println);

        System.out.println("----------");
        //每隔一段时间抽取样本数(取在这个时间的第一个元素)，如果相隔实现大于序列的执行时间，则取第一个元素
        Flux.range(100, 10).sampleFirst(Duration.ofMillis(100)).subscribe(System.out::println);

        System.out.println("----------");
        //只获取一个元素，single()如果为空或者超多一个，抛出异常，single(1)如果为空返回默认值，如果多个抛出异常,singleOrEmpty()可以允许为空
        Flux.range(100, 10).single(1).subscribe(System.out::println);
    }


    /**
     * 当被订阅后如果发生异常，则stream会停止运行
     * 此时可以通过处理error来决定如何处理异常
     * 可以将异常跳过、将异常替换等
     */
    @Test
    public void testErrorHandle() {
        System.out.println("----------");
        Flux.just(1, 2, 3, 0, 5, 4).map(it -> {
                    it = 100 / it;
                    return it;
                })
                //报错后返回，并停止运行
                .onErrorResume(e -> {
                    return Mono.just(10000);
                })
                .doFinally(type -> {
                    System.out.println(type);
                })
                .subscribe(System.out::println);

        System.out.println("----------");
        Flux.just(1, 2, 3).doOnNext(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
                if (integer == 2) {
                    throw new RuntimeException("触发异常");
                }
            }
        }).doOnError(new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                System.out.println("doOnError：" + throwable.getMessage());
            }
        }).subscribe();


        System.out.println("----------");
        Flux.just(1, 2, 3, 0, 5, 4).map(it -> {
                    it = 100 / it;
                    return it;
                })
                //报错后继续运行，并执行相关操作
                .onErrorContinue((e, it) -> {
                    System.out.println(e.getMessage());
                })
                .doFinally(type -> {
                    System.out.println(type);
                })
                .subscribe(System.out::println);
    }

    @Test
    public void flatMapTest() {
        //输出50，100
        Flux.just(5, 10).flatMap(x -> Flux.just(x * 10)).toStream().forEach(System.out::println);


    }
}
