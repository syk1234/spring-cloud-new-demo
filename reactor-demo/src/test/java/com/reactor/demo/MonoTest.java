package com.reactor.demo;

import org.junit.Test;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class MonoTest {
    /**
     * 基本用法
     * 1、执行顺序：onNext依次执行
     * 2、subscribe中有三个入参，分别是consumer,errorConsumer,completeRunnable
     * 2.1、正常情况下，会先执行consumer，然后再执行complete runnable
     * 2.2、如果在执行onNext过程中，产生异常，不会进入complete runnable。直接由subscribe中的errorConsumer来处理。
     * <p>
     * 输出内容如下：
     * doOnNext1:hello world
     * doOnNext2:hello world
     * subscribe:hello world
     * subscribe complete
     */
    @Test
    public void test0() {
        //just用法
        Mono.just("hello world")
                .doOnNext(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println("doOnNext1:" + s);
                    }
                }).doOnNext(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println("doOnNext2:" + s);
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println("subscribe:" + s);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        System.out.println("subscribe exception:" + throwable.getMessage());
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("subscribe complete");
                    }
                });
    }

    /**
     * 异常产生，降级处理
     * 这里手动抛出了一个异常，但是onErrorResume作为降级，把异常吃了。所以这里能正常complete
     * 输出内容如下：
     * <p>
     * doOnNext1:hello world
     * onErrorResume:手动抛异常
     * subscribe:我又复活了
     * subscribe complete
     */
    @Test
    public void testFallBack() {
        //just用法
        Mono.just("hello world")
                .doOnNext(s -> System.out.println("doOnNext1:" + s)).doOnNext(s -> {
                    throw new RuntimeException("手动抛异常");
                }).onErrorResume(throwable -> {
                    System.out.println("onErrorResume:" + throwable.getMessage());
                    return Mono.just("我又复活了");
                })
                .subscribe(s -> System.out.println("subscribe:" + s),
                        throwable -> System.out.println("subscribe exception:" + throwable.getMessage()),
                        () -> System.out.println("subscribe complete"));
    }

    @Test
    public void test1() {
        //runnable创建mono
        Mono<Void> sinkMono = Mono.fromRunnable(() -> System.out.println("runnable"));
        //这句不会输出
        sinkMono.doOnNext(unused -> System.out.println("void success"));
        //这句也不会输出
        sinkMono.subscribe(o -> System.out.println("void result" + o));

        //创建一个不包含任何元素，只发布结束消息的序列。，这里的hello empty是不会输出的。
        Mono.empty()
                //输出“empty的入参是null”
                .doOnSuccess(o -> System.out.println("empty的入参是" + o))
                //这句不会输出
                .subscribe(o -> System.out.println("hello empty"));
        //empty里面至少还有一个结束消息，而never则是真的啥都没有。"never的入参是"不会输出 ，这里的hello never也不会输出
        Mono.never().doOnSuccess(o -> System.out.println("never的入参是" + o)).subscribe(o -> System.out.println("hello never"));

        //先后输出success和then
        Mono<Void> empty = Mono.empty();
        empty.doOnSuccess(avoid -> System.out.println("success:" + avoid))
                .onErrorResume(throwable -> {
                    System.out.println(throwable.getMessage());
                    return Mono.error(throwable);
                })
                .then(Mono.defer(() -> {
                    System.out.println("then");
                    return Mono.empty();
                })).subscribe();
    }

    @Test
    public void test2() {
        System.out.println("-------");

        //传入supplier
        System.out.println("-------");
        Mono.fromSupplier(() -> "Hello supplier").subscribe(System.out::println);
        //传入optional
        System.out.println("-------");
        Mono.justOrEmpty(Optional.of("Hello optional")).subscribe(System.out::println);
        //通过sink来创建一个正常执行的Mono
        System.out.println("-------");
        Mono.create(sink -> sink.success("Hello sink")).subscribe(System.out::println);
        //通过sink来创建一个抛出异常的Mono
        System.out.println("-------");
        Mono.create(sink -> sink.error(new RuntimeException("sink error"))).subscribe(System.out::println);
        //defer的入参实际上是一个Mono工厂
        System.out.println("-------");
        Mono.defer(() -> Mono.just("hello defer")).subscribe(System.out::println);
    }

    @Test
    public void test3() {
        //callable，有返回值
        Mono.fromCallable(() -> "callable").subscribe(System.out::println);
        //runnable无返回值
        Mono<Void> mono = Mono.fromRunnable(() -> System.out.println("run"));
        //下面的hello runnable是不会输出的。因为subscribe一个Mono<Void>,不会产生任何结果
        mono.subscribe(o -> System.out.println("hello runnable"));
    }

    @Test
    public void test4() {
        //延迟3秒输出
        Mono.delay(Duration.ofSeconds(3)).doOnNext(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) {
                System.out.println(aLong);
            }
        }).block();

    }

    @Test
    public void test5() {
        //直接输出了异常
        Mono.error(new RuntimeException("这是一个异常")).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) {
                System.out.println("error:" + o);
            }
        });

        Mono.defer(() -> {
            return Mono.error(new RuntimeException("这是第二个异常"));
        }).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) {
                System.out.println("defer error:" + o);
            }
        });
    }

    @Test
    public void test6() {
        //通过map可以对元素进行转换
        Mono.just("just one").map(new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return 1;
            }
        }).doOnNext(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println("转换后的结果：" + integer);
            }
        }).subscribe();
    }
}
